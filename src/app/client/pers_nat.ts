import { nationalite } from "./nationalite";
import { pays } from "./pays";

export class pers_nat 
{   code_nat_pers:number;
    pers1 :number;
    code_nationalite:number;
    code_nat:number;
    nom:string;
    listePays=Array<pays>();
    
}