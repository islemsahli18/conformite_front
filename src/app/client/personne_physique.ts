import { contact } from "./contact";
import { parente } from "./parente";
import { pers_nat } from "./pers_nat";
import { revenu } from "./revenu";

export class personne_physique extends contact{
    Nom:String;
    Prenom:String;
    Date_naiss:Date;
    us_permenant:Boolean;
    Num_passport:String;
    num_cin:number;
    Etat_civil:String;
    Sexe:String;
    C_juridique:String; //capacite juridique
    Cat_sociopro:String ; // categorie socioprofessionnelle
    Profession:String;
    pays_naiss:String;
    cat_employeur:String;  //Catégorie Employeur
    secteur_travail:String;
    contrat_travail:String;
    Date_Tit:String ; //date_Titularisation
    Pays_travail:String;
    num_RNE:number;
    date_extrait_rne:Date;
    regime_fiscal:String;
    matricule_fiscal:String;
    code_douane:String;
    aff_soc:String; //affiliation social: CNSS/CNRPS
    num_aff:Number;
    sit_habitat:String; //SITUATION D'HABITATION!
    Reg_mat:String; //Régime_matrimonial
    lien_parente:Boolean;
    statut_pep:Boolean;
    carte_verte:Boolean;
    carte_sejour:String;
    delais_d_cin:Date; //delais date debut
    delais_f_cin:Date; //delais date fin
    delais_d_pass:Date;
    delais_f_pass:Date;
    delais_d_carte:Date;//delais date debut de carte sejour
    delais_f_carte:Date;
    statut_fatca:Boolean;
    listRevenu=Array<revenu>();
    listPers_par=Array<parente>();
    listPers_nat1=Array<pers_nat>();
}
