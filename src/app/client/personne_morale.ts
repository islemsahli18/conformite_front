import { contact } from "./contact";

export class personne_morale extends contact {

    DS :String ; //Denomination_sociale
    OS :String ;// Objet_social
    Num_id :String ;
    Date_const:Date;
    Pays_const:String;
    Date_enreg:Date ;
    Associés_PPE:Boolean;
    Nature_Operation:String;
    BrèveDescription_NO:String;
    Origine_Fonds:String;
    OF_T:Boolean ;
    Pays_OF:String ;
    Nature_PM :String; //nature personne morale
    Domaine_Act:String;
    Ancienneté_pro:String ;
    Forme_juri:String ;
}