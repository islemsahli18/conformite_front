import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TableListComponent } from './table-list/table-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';
import {
  AgmCoreModule
} from '@agm/core';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { DetailsComponent } from './table-list/details/details.component';
import { MaterialModule } from './material/material.module';
import { RevenuComponent } from './user-profile/revenu/revenu.component';
import { PEPComponent } from './user-profile/pep/pep.component';
import { NationnaliteComponent } from './user-profile/nationnalite/nationnalite.component';
import { ParenteComponent } from './user-profile/parente/parente.component';
import { UpdateComponent } from './table-list/update/update.component';



@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    MaterialModule
    

   
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    DetailsComponent,
    RevenuComponent,
    PEPComponent,
    NationnaliteComponent,
    ParenteComponent,
    UpdateComponent
   
   
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[RevenuComponent, PEPComponent]
})
export class AppModule { }
