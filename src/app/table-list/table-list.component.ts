import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { PersonnePhysiqueService } from 'app/service_clients/personne-physique.service';



@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {

  personne_physique:any;
  code:Number;
  
  constructor( private service:PersonnePhysiqueService,private router: Router) { }

  ngOnInit(): void {
    
    this.reloadData();}
  
  reloadData() {
    let resp=this.service.getPPS();
    resp.subscribe((data)=>this.personne_physique=data);
  }

  public findId(){
    let resp= this.service.getPP(this.personne_physique.Code_clt);
    resp.subscribe((data)=>this.personne_physique=data);
   }

   details(id: number){
    this.router.navigate(['/details', id])
  }

     
   updatePP(id: number){
    this.router.navigate(['/Personne/UpdatePersonnePhysisque']);
  }
  AjoutPP(id: number)
  {this.router.navigate(['/Personne/CreatePersonnePhysique'])}
  
  public deletePP(id:number){
    let resp= this.service.deletePP(id);
    resp.subscribe((data)=> {
      console.log(data);
      this.reloadData();
    },
    error => console.log(error));}
    
  }
   

