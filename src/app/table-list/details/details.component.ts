import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { personne_physique } from 'app/client/personne_physique';
import { pers_nat } from 'app/client/pers_nat';
import { NationaliteService } from 'app/service_clients/nationalite.service';
import { PEPService } from 'app/service_clients/pep.service';
import { PersNatService } from 'app/service_clients/pers-nat.service';
import { PersonnePhysiqueService } from 'app/service_clients/personne-physique.service';
import { RevenuService } from 'app/service_clients/revenu.service';
import { serviceconsumermanagement_v1 } from 'googleapis';
import { data } from 'jquery';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  personne_physique:any;
  revenu :any;
  pep:any;
  id:number;
  pers_nat:any;
pers1:personne_physique;

 
  constructor(private service:PersonnePhysiqueService, private service1:RevenuService,private service4:NationaliteService,private service2:PEPService ,private service3:PersNatService ,private router: Router,private route: ActivatedRoute) { }
  
 
  ngOnInit() {
    
    this.id = this.route.snapshot.params['id'];
   


   
    
  
    
    this.service.getPP(this.id)
    .subscribe(data => {
        console.log(data)
        this.personne_physique = data;
      }, error => console.log(error));

      this.service1.getR(this.id)
      .subscribe(data => {
          console.log(data)
          this.revenu= data;
        }, error => console.log(error));

        this.service3.getPPS(this.id)
        .subscribe(data => {
            console.log(data)
            this.pers_nat= data;
          }, error => console.log(error));
      
        this.service2.getPP(this.id)
        .subscribe(data => {
            console.log(data)
            this.pep= data;
          }, error => console.log(error));
      
          this.service4.getPP(this.pers_nat.code_nationalite)
          .subscribe(data => {
              console.log(data)
              this.pep= data;
            }, error => console.log(error));
    
  }
 
  details(id: number){
    this.router.navigate(['/update', id])
  }

}
