import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { personne_physique } from 'app/client/personne_physique';
import { revenu } from 'app/client/revenu';
import { PersonnePhysiqueService } from 'app/service_clients/personne-physique.service';
import { RevenuService} from 'app/service_clients/revenu.service';

import { FormControl, Validators } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { RevenuComponent } from './revenu/revenu.component';
import { PEPComponent } from './pep/pep.component';
import { NationnaliteComponent } from './nationnalite/nationnalite.component';
import { ParenteComponent } from './parente/parente.component';
import { liste_nat } from 'app/client/liste_nat';
import { FatcaService } from 'app/service_clients/fatca.service';
import { fatca } from 'app/client/fatca';
import { secteur } from 'app/client/secteur';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
 
   
   message:any;
   message1:any;
   personne_physique:personne_physique=new personne_physique();
   revenu :revenu =new revenu();
   v:number;
   liste_nationnalite=liste_nat;
  test: Boolean;
  fatca: fatca;


  pays: secteur[] = [
    { name: 'Agroalimentaire' },
    {  name: 'Assurance' },
    {  name: 'Automobile-Reparation automobiles' },
    {  name: 'Arment'},
    {  name: 'Bnaque'},
    {  name: 'Bois-Carton' },
    {  name: 'BTP-Materiaux de construction' },
    {  name: 'Bureaux de change'},
    {  name: 'Chaussure' },
    { name: 'Clubs VIP'},
    {  name: 'Casinos'},
    {  name: 'Commerce'},
    {  name: 'Chimie-Parachimie' },
    {  name: 'Distribution'},
    { name: 'Droit'},
    {  name: 'Electronique' },
    {  name: 'Ecletricite' },
    {  name: 'Enseignement' },
    {  name: 'Environnement' },
    {  name: 'Etudes et conseils'},
    {  name: 'Etablissement de jeux de hasard'},
    {  name: 'Hotellerie-Restauration'},
    {  name: 'Industrie pharmaceutique' },
    {  name: 'Machine et equipement' },
    {  name: 'Marketing'},
    {  name: 'Metaux et pierres precieuse'},
    {  name: 'Métallurgie-Travail du métal'},
    {  name: 'Informatique-Telecoms'},
    {  name: 'Import-Export'},
    {  name: 'Industrie petro-chimique'},
    {  name: 'Immobilier'},
    {  name: 'Industrie'},
    {  name: 'Jeu Video'},
    {  name: 'Journalisme'},
    {  name: 'Langues'},
    {  name: 'Papier-Imprimerie' },
    { name: 'Petrole'},
    {  name: 'Plastique-Caoutchouc' },
    {  name: 'Ressource Humaine' },
    {  name: 'Relations d’affaire avec etats ou gouvernements etrangers'},
    {  name: 'Services aux entreprises'},
    { name: 'Textile-Habillement' },
    {  name: 'Transport maritime'},
    {  name: 'Tourisme'},
    {  name: 'Transport-Logistique'},
   
  ];


  profession: secteur[] = [
    { name: 'Architecte' },
    {  name: 'Artisan' },
    {  name: 'Auteur' },
    {  name: 'Agent immobilier'},
    {  name: 'Agent du location'},
    {  name: 'Agent de joueurs' },
    {name: 'Avocat'},
    {  name: 'Artiste'},
    {  name: 'Boulanger' },
    {  name: 'Biologiste'},
    {  name: 'Chauffeur' },
    {  name: 'Comedien'},
    {  name: 'Comptable' },
    {  name: 'Concierge'},
    {  name: 'Comedien'},
    {  name: 'Conseiller'},
    {  name: 'Cuisinier' },
    {  name: 'Caissier'},
    {  name: 'Chef projet'},
    {  name: 'Chef d agence'},
    {  name: 'Casinos'},
    {  name: 'Commercant'},
    {  name: 'Directeur'},
    {  name: 'Developpeur'},
    {  name: 'Enseignant' },
    {  name: 'Formateur' },
    {  name: 'Geographe' },
    {  name: 'Gerant' },
    {  name: 'Graphiste'},
    {  name: 'Guide'},
    {  name: 'Illustrateur'},
    {  name: 'Infermier' },
    {  name: 'Ingenieur' },
    {  name: 'Jardinier'},
    {  name: 'Libraire'},
    {  name: 'Médecin'},
    {  name: 'Realisateur'}, 
    {  name: 'Responsable ressource humaine' },
    {  name: 'Serveur'},
    {  name: 'Traducteur'},
    {  name: 'Vendeur'},
    {  name: 'Vendeurs et revendeur de bateaux'},
    {  name: 'Homme d affaire'},
    {  name: 'Femme d affaire' },
    {  name: 'Sans Activite'},   
    {  name: 'Tresorier d une association' },
    {  name: 'President d une association'},

  
    
   
  ];
  
  constructor(private service:PersonnePhysiqueService, private service1:FatcaService,private router: Router ,private snackBar: MatSnackBar,private dialog: MatDialog,) { }


  
  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
   
  ngOnInit(): void {
  }

  public CreateNow()
  {
    
      
    let resp= this.service.updatePP(this.personne_physique.code_clt,this.personne_physique);
    resp.subscribe((data)=>this.message=data);
    
    let snackBarRef = this.snackBar.open('Client cree!', 'Bravo', {
      duration: 3000
    });
   
  }
     //gotoList() {
      //this.router.navigate(['/Candidat/list']);
    //}
  


      onClick(Code_clt:number) {  
      const dialogConfig = new MatDialogConfig();
       dialogConfig.disableClose = true;
       dialogConfig.autoFocus = true;   
       dialogConfig.data = { Code_clt };
       dialogConfig.width = "60%";
       this.dialog.open(RevenuComponent,dialogConfig);
      } 


       onClick1(Code_clt:number) {  
       const dialogConfig = new MatDialogConfig();
       dialogConfig.disableClose = true;
       dialogConfig.autoFocus = true;   
       dialogConfig.data = { Code_clt };
       dialogConfig.width = "60%";
       this.dialog.open(PEPComponent,dialogConfig);
      } 

     onClick2(Code_clt:number) {  
      let resp= this.service.CreatePP(this.personne_physique);
      resp.subscribe((data)=>this.message=data);
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;   
      dialogConfig.data = { Code_clt };
      dialogConfig.width = "60%";
      this.dialog.open(NationnaliteComponent,dialogConfig);
      this.test=this.personne_physique.us_permenant;
      
      }

      
     onClick3(Code_clt:number) {  
      let resp= this.service.CreatePP(this.personne_physique);
      resp.subscribe((data)=>this.message=data);
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;   
      dialogConfig.data = { Code_clt };
      dialogConfig.width = "60%";
      this.dialog.open(ParenteComponent,dialogConfig);
    
      }

}
