import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { pep } from 'app/client/pep';
import { pays } from 'app/client/pays';
import { PEPService } from 'app/service_clients/pep.service';
import { liste_nat } from 'app/client/liste_nat';
@Component({
  selector: 'app-pep',
  templateUrl: './pep.component.html',
  styleUrls: ['./pep.component.css']
})
export class PEPComponent implements OnInit {

 
  
  id1=20;
  pep :pep =new pep();
  message:any;
  id:number;
  liste_nationnalite=liste_nat;

  constructor(@Inject(MAT_DIALOG_DATA) public data1, private service:PEPService,private snackBar: MatSnackBar,public dialogRef: MatDialogRef<PEPComponent>) { }

 
  ngOnInit(): void {                                                                                                    
    this.id=this.data1.Code_clt;
    this.pep.contact=this.id;   
  }


  OnCreatePEP ()
  {let snackBarRef = this.snackBar.open('PEP est ajoutée!', 'Bravo', {
    duration: 3000
  });
  
  let resp= this.service.CreatePP(this.pep);
     resp.subscribe((data)=>this.message=data);
     this.dialogRef.close();
  }

  Close()
  {this.dialogRef.close();}
}
