import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { nationalite } from 'app/client/nationalite';
import { liste_nat } from 'app/client/liste_nat';
import { pers_nat } from 'app/client/pers_nat';
import { NationaliteService } from 'app/service_clients/nationalite.service';
import {  PersNatService } from 'app/service_clients/pers-nat.service';

@Component({
  selector: 'app-nationnalite',
  templateUrl: './nationnalite.component.html',
  styleUrls: ['./nationnalite.component.css']
})
export class NationnaliteComponent implements OnInit {

  liste_nationnalite=liste_nat;
  
  id1=20;
  pers_nat :pers_nat =new pers_nat();
 
  message:any;
  id:number;
 

  constructor(@Inject(MAT_DIALOG_DATA) public data1, private service1:NationaliteService,private service:PersNatService,private snackBar: MatSnackBar,public dialogRef: MatDialogRef<NationnaliteComponent>) { }

 
  ngOnInit(): void {                                                                                                    
    this.id=this.data1.Code_clt;
    this.pers_nat.pers1=this.id;   
   
  
  }


  OnCreateNAT()
  {let snackBarRef = this.snackBar.open('nationalite est ajoutée!', 'Bravo', {
    duration: 3000
  });
 
  let resp= this.service.CreatePP(this.pers_nat);
     resp.subscribe((data)=>this.message=data);
     
  }

  Close()
  {this.dialogRef.close();}

}
