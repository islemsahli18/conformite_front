import { TestBed } from '@angular/core/testing';

import { FatcaService } from './fatca.service';

describe('FatcaService', () => {
  let service: FatcaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FatcaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
