import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { nationalite } from 'app/client/nationalite';

@Injectable({
  providedIn: 'root'
})
export class NationaliteService {

  constructor(private http:HttpClient) { }
  public CreateNAT(PP:nationalite)
  { return this.http.post("http://localhost:8080/Nationalite/Create",PP,{responseType:'text' as 'json'})}

  public getPPS()
  {return this.http.get("http://localhost:8080/Pep/GetAll")}

  public deletePP(id:number)
  {return this.http.delete("http://localhost:8080/Pep/Delete/"+id,{responseType:'text' as 'json'})
}
  public updatePP(id:number, PP:nationalite)
  {
    return this.http.put("http://localhost:8080/Pep/Update/"+id,PP,{responseType:'text' as 'json'})
  }

  public getPP(id:number)
  {return this.http.get("http://localhost:8080/Pep/Get/"+id)}
}
