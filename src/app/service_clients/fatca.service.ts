import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { fatca } from 'app/client/fatca';

@Injectable({
  providedIn: 'root'
})
export class FatcaService {

  constructor(private http:HttpClient) { }
  public CreatePP(PP:fatca)
  { return this.http.post("http://localhost:8080/PersonnePhysique/Create",PP,{responseType:'text' as 'json'})}

  public getPPS()
  {return this.http.get("http://localhost:8080/PersonnePhysique/GetAll")}

  public deletePP(id:number)
  {return this.http.delete("http://localhost:8080/PersonnePhysique/Delete/"+id,{responseType:'text' as 'json'})
}
  public updatePP(id:number, PP:fatca)
  {
    return this.http.put("http://localhost:8080/Fatca/Update/"+id,PP,{responseType:'text' as 'json'})
  }

  public getPP(id:number)
  {return this.http.get("http://localhost:8080/PersonnePhysique/Get/"+id,{responseType:'text' as 'json'})}
}
