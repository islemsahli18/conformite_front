import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { pep} from 'app/client/pep';

@Injectable({
  providedIn: 'root'
})
export class PEPService {

  constructor(private http:HttpClient) { }
  public CreatePP(PP:pep)
  { return this.http.post("http://localhost:8080/PEP/Create",PP,{responseType:'text' as 'json'})}

  public getPPS()
  {return this.http.get("http://localhost:8080/Pep/GetAll")}

  public deletePP(id:number)
  {return this.http.delete("http://localhost:8080/Pep/Delete/"+id,{responseType:'text' as 'json'})
}
  public updatePP(id:number, PP:pep)
  {
    return this.http.put("http://localhost:8080/Pep/Update/"+id,PP,{responseType:'text' as 'json'})
  }

  public getPP(id:number)
  {return this.http.get("http://localhost:8080/PEP/GetBE/"+id)}
}
