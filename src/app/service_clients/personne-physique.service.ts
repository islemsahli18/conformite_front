import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { personne_physique } from 'app/client/personne_physique';

@Injectable({
  providedIn: 'root'
})
export class PersonnePhysiqueService {

  constructor(private http:HttpClient) { }
  public CreatePP(PP:personne_physique)
  { return this.http.post("http://localhost:8080/PersonnePhysique/Create",PP,{responseType:'text' as 'json'})}

  public getPPS()
  {return this.http.get("http://localhost:8080/PersonnePhysique/GetAll")}

  public deletePP(id:number)
  {return this.http.delete("http://localhost:8080/PersonnePhysique/Delete/"+id,{responseType:'text' as 'json'})
}
  public updatePP(id:number, PP:personne_physique)
  {
    return this.http.put("http://localhost:8080/PersonnePhysique/Update/"+id,PP)
  }

  public getPP(id:number)
  {return this.http.get("http://localhost:8080/PersonnePhysique/Get/"+id)}
}

