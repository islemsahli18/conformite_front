import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { pers_nat } from 'app/client/pers_nat';

@Injectable({
  providedIn: 'root'
})
export class PersNatService {

  constructor(private http:HttpClient) { }
  public CreatePP(PP:pers_nat)
  { return this.http.post("http://localhost:8080/Pers_Nat/Create",PP,{responseType:'text' as 'json'})}

  public getPPS(id:number)
  {return this.http.get("http://localhost:8080/Pers_Nat/GetAll/"+id)}

  public deletePP(id:number)
  {return this.http.delete("http://localhost:8080/Pep/Delete/"+id,{responseType:'text' as 'json'})
}
  public updatePP(id:number, PP:pers_nat)
  {
    return this.http.put("http://localhost:8080/Pep/Update/"+id,PP,{responseType:'text' as 'json'})
  }

  public getPP(id:number)
  {return this.http.get("http://localhost:8080/Pep/Get/"+id,{responseType:'text' as 'json'})}
}
