import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { revenu } from 'app/client/revenu';

@Injectable({
  providedIn: 'root'
})
export class RevenuService {

  constructor(private http:HttpClient) { }
  public CreateR(R:revenu)
  { return this.http.post("http://localhost:8080/Revenu/Create",R,{responseType:'text' as 'json'})}

  public getRs()
  {return this.http.get("http://localhost:8080/Revenu/GetAll")}

  public deleteR(id:number)
  {return this.http.delete("http://localhost:8080/Revenu/Delete/"+id,{responseType:'text' as 'json'})
}
  public updateR(id:number, R:revenu)
  {
    return this.http.put("http://localhost:8080/Revenu/Update/"+id,R,{responseType:'text' as 'json'})
  }

  public getR(id:number)
  {return this.http.get("http://localhost:8080/Revenu/Get/"+id)}
}
